package am.matveev.spring.PhoneNumber.models;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "Contact")
@Data
public class ContactEntity{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

//    private List<PhoneNumber>
}
