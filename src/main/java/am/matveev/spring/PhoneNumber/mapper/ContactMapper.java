package am.matveev.spring.PhoneNumber.mapper;

import am.matveev.spring.PhoneNumber.dto.ContactDTO;
import am.matveev.spring.PhoneNumber.models.ContactEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ContactMapper{

    ContactDTO toDTO(ContactEntity contact);
    ContactEntity toEntity(ContactDTO contactDTO);
}
