package am.matveev.spring.PhoneNumber.services;

import am.matveev.spring.PhoneNumber.dto.ContactDTO;
import am.matveev.spring.PhoneNumber.mapper.ContactMapper;
import am.matveev.spring.PhoneNumber.models.ContactEntity;
import am.matveev.spring.PhoneNumber.repositories.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContactService{

    private final ContactRepository contactRepository;
    private final ContactMapper contactMapper;

    @Autowired
    public ContactService(ContactRepository contactRepository, ContactMapper contactMapper){
        this.contactRepository = contactRepository;
        this.contactMapper = contactMapper;
    }

    @Transactional(readOnly = true)
    public List<ContactDTO> findAll(){
        List<ContactEntity> contact = contactRepository.findAll();
        List<ContactDTO> contactDTOList = contact.stream()
                .map(contactMapper::toDTO)
                .collect(Collectors.toList());
        return contactDTOList;
    }

    @Transactional(readOnly = true)
    public ContactDTO findOne(long id){
        ContactEntity contact = contactRepository.findById(id)
                .orElse(null);
        return contactMapper.toDTO(contact);
    }

    public ContactDTO create(ContactDTO contactDTO){
        ContactEntity contact = contactMapper.toEntity(contactDTO);
        contactRepository.save(contact);
        return contactMapper.toDTO(contact);
    }

    public ContactDTO update(long id, ContactDTO contactDTO){
        ContactEntity contact = contactMapper.toEntity(contactDTO);
        contact.setId(id);
        contactRepository.save(contact);
        return contactMapper.toDTO(contact);
    }

    public void delete(long id){
        contactRepository.deleteById(id);
    }
}
