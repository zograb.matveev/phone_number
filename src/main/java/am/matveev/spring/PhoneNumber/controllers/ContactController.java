package am.matveev.spring.PhoneNumber.controllers;

import am.matveev.spring.PhoneNumber.dto.ContactDTO;
import am.matveev.spring.PhoneNumber.models.ContactEntity;
import am.matveev.spring.PhoneNumber.services.ContactService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contact")
public class ContactController{

    private final ContactService contactService;

    @Autowired
    public ContactController(ContactService contactService){
        this.contactService = contactService;
    }

    @GetMapping()
    public List<ContactDTO> FindAll(){
        return contactService.findAll();
    }

    @GetMapping("/{id}")
    public ContactDTO findOne(@PathVariable long id){
        return contactService.findOne(id);
    }

    @PostMapping("/create")
    public ResponseEntity<HttpStatus> createContact(@RequestBody @Valid ContactDTO contactDTO){
        contactService.create(contactDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<HttpStatus> updateContact(@PathVariable long id,@RequestBody @Valid ContactDTO contactDTO){
        contactService.update(id,contactDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteContact(@PathVariable long id){
        contactService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
