package am.matveev.spring.PhoneNumber.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactDTO{

    @NotEmpty(message = "FirstName should not be empty")
    private String firstName;

    @NotEmpty(message = "LastName should not be empty")
    private String lastName;

    @Email
    @NotEmpty(message = "Email should not be empty")
    private String email;

    @NotEmpty(message = "Password should not be empty")
    private String password;
}
