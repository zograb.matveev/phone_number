package am.matveev.spring.PhoneNumber.repositories;

import am.matveev.spring.PhoneNumber.models.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<ContactEntity,Long>{
}
